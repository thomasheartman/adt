module Option

open FsCheck.Xunit
open System

[<Property>]
let ``My test`` (b : bool) =
    b = b