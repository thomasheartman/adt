using System;

namespace ADT
{
    public abstract class Option<T>
    {
        private Option() { }

        public bool IsSome => this is Some;
        public bool IsNone => !IsSome;

        public static Some MakeSome(T t) => new Some(t);
        public static None MakeNone() => new None();

        public sealed class Some : Option<T>
        {
            public T Value { get; }
            internal Some(T value)
            {
                Value = value;
            }

        }

        public sealed class None : Option<T>
        {
            internal None() { }
        }


        public U Match<U>(Func<T, U> onSome, Func<U> onNone)
        {
            switch (this)
            {
                case Option<T>.Some x:
                    return onSome(x.Value);
                case Option<T>.None _:
                    return onNone();
                default:
                    throw new ArgumentException(message: $"I got an unrecognized subclass of Option: {this}");
            }
        }


        public void Match(Action<T> onSome, Action onNone)
        {
            switch (this)
            {
                case Option<T>.Some x:
                    onSome(x.Value);
                    return;
                case Option<T>.None _:
                    onNone();
                    return;
                default:
                    throw new ArgumentException(message: $"I got an unrecognized subclass of Option: {this}");
            }
        }

        public Option<U> Bind<U>(Func<T, Option<U>> f) =>
            Match<Option<U>>(
                f,
                Option<U>.MakeNone
            );

        public Option<U> Map<U>(Func<T, U> f) =>
            Match<Option<U>>(
                x => Option<U>.MakeSome(f(x)),
                Option<U>.MakeNone
            );
    }
}
