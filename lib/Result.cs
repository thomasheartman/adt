using System;

namespace ADT
{
    public abstract class Result<T, E>
    {
        private Result() { }

        public bool IsOk => this is Ok;
        public bool IsErr => !IsOk;

        public static Err MakeErr(E e) => new Err(e);
        public static Ok MakeOk(T t) => new Ok(t);

        public sealed class Ok : Result<T, E>
        {
            public T Value { get; }
            internal Ok(T value)
            {
                Value = value;
            }

        }

        public sealed class Err : Result<T, E>
        {
            public E Value { get; }
            internal Err(E value)
            {
                Value = value;
            }
        }

        public U Match<U>(Func<T, U> onOk, Func<E, U> onError)
        {
            switch (this)
            {
                case Ok ok:
                    return onOk(ok.Value);
                case Err e:
                    return onError(e.Value);
                default:
                    throw new ArgumentException(message: $"I got an unrecognized subclass of Result: {this}");
            }
        }


        public void Match(Action<T> onOk, Action<E> onError)
        {
            switch (this)
            {
                case Ok ok:
                    onOk(ok.Value);
                    return;
                case Err e:
                    onError(e.Value);
                    return;
                default:
                    throw new ArgumentException(message: $"I got an unrecognized subclass of Result: {this}");
            }
        }

        public Result<U, E> Bind<U>(Func<T, Result<U, E>> f) =>
            Match<Result<U, E>>(
                f,
                Result<U, E>.MakeErr
            );

        public Result<U, E> Map<U>(Func<T, U> f) =>
            Match<Result<U, E>>(
                x => Result<U, E>.MakeOk(f(x)),
                Result<U, E>.MakeErr
            );
    }
}
